# FileManager

## Fast start

### On first start

  ``` npm install && npm run start ```

### Next time

 ```npm run start ```

Server started on http://localhost:8080/

## Build

  ``` npm install && npm run build-prod ```

The assembled project is in the dist folder
